import Game from './game'
import Score from './score-board'
import NewGame from './newGame'

export {Score, Game, NewGame}