import React, {useState, useEffect} from 'react'
import {Board} from '../styles'
import {FaHandRock, FaHandPaper, FaHandScissors} from 'react-icons/fa'
import {VscLoading} from 'react-icons/vsc'

const Game = (
    {countWin, countDraw, countLoose, 
    setCountWin, setCountDraw, setCountLoose,
    setVisible, setTurn, yourTurn, yourChoice, 
    macChoice, setYourChoice, setMacChoice}) => {
        
    const [messageVictory, setMessageVictory] = useState('Playing')
    
    const handleClick = (n) => {
        setYourChoice(n)
        setTurn(false)
        setTimeout(() => setVisible(true), 500)
    }

    useEffect(() => {
        if (yourChoice === undefined) {
            return
        }
        const x = Math.ceil(Math.random() * 3)
        setTimeout(() => setMacChoice(x), 1000)
        
    }, [yourChoice])

    useEffect(() => {
        if (macChoice === 0) {
            return
        }

        if (yourChoice === 1 && macChoice === 3 || yourChoice === 2 && macChoice === 1 || yourChoice === 3 && macChoice === 2) {
            setMessageVictory('You win')
            setCountWin(countWin + 1)
            return
        } else if (yourChoice === 1 && macChoice === 2) {
            setMessageVictory('Computer wins')
            setCountLoose(countLoose + 1)
            return
        } else if (yourChoice === 2 && macChoice === 3) {
            setMessageVictory('Computer wins')
            setCountLoose(countLoose + 1)
            return
        } else if (yourChoice === 3 && macChoice === 1) {
            setMessageVictory('Computer wins')
            setCountLoose(countLoose + 1)
            return
        } else if (yourChoice === macChoice){
            setMessageVictory('Draw')
            setCountDraw(countDraw + 1)
            return
        }
    }, [macChoice])
    

    let YourChoice = ''
    if (yourChoice === 1) {
        YourChoice = FaHandRock
    } if (yourChoice === 2) {
        YourChoice = FaHandPaper
    } if (yourChoice === 3) {
        YourChoice = FaHandScissors
    }

    let MacChoice = ''
    if (macChoice === 0) {
        MacChoice = VscLoading
    } if (macChoice === 1) {
        MacChoice = FaHandRock
    } if (macChoice === 2) {
        MacChoice = FaHandPaper
    } if (macChoice === 3) {
        MacChoice = FaHandScissors
    }

    console.log(yourChoice, macChoice, messageVictory)

    if (yourTurn) {
        return (
            <>
            <Board>
                <h1>Your turn</h1>
                <div onClick={() => {handleClick(1)}}>
                    <p><FaHandRock /></p>
                    <span>Rock</span>
                </div>
                <div onClick={() => {handleClick(2)}}>
                    <p><FaHandPaper /></p>
                    <span>Paper</span>
                </div>
                <div onClick={() => {handleClick(3)}}>
                    <p><FaHandScissors /></p>
                    <span>Scissor</span>
                </div>
            </Board>
            </>
        )

    }

    return (
        <>
        <Board>
            <h1>{messageVictory}</h1>
            <div>
                <p>{<YourChoice />}</p>
                <span>Your</span>
            </div>

            <div>
                <p>{<MacChoice />}</p>
                <span>Machine</span>
            </div>
        </Board>
        </>        
    )
}

export default Game