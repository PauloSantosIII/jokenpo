import React from 'react'
import {Board} from '../styles'

const Score = ({countWin, countDraw, countLoose}) => {
    return (
        <Board>
            <h1>Placar</h1>
            <div>
                <p>{countWin}</p>
                <span>You</span>
            </div>
            <div>
                <p>{countDraw}</p>
                <span>Draw</span>
            </div>
            <div>
                <p>{countLoose}</p>
                <span>Machine</span>
            </div>
        </Board>
    )
}

export default Score