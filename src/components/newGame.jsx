import React from 'react'
import {StyledButton} from '../styles'

const NewGame = ({setVisible, setTurn, setYourChoice, setMacChoice}) => {
    const handleYes = () => {
        setTurn(true)
        setVisible(false)
        setYourChoice(undefined)
        setMacChoice(0)
    }

    return (
        <StyledButton onClick={handleYes}>
            Play Again?
        </StyledButton>
    )
}

export default NewGame