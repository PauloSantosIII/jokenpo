import React, {useState} from 'react'
import {BodyStyle, Title} from './styles'
import {Score, Game, NewGame} from './components'

function App() {
  const [countWin, setCountWin] = useState(0)
  const [countDraw, setCountDraw] = useState(0)
  const [countLoose, setCountLoose] = useState(0)
  const [yourChoice, setYourChoice] = useState(undefined)
  const [macChoice, setMacChoice] = useState(0)
  const [visible, setVisible] = useState(false)
  const [yourTurn, setTurn] = useState(true)


  return (
    <BodyStyle>
      <Title>JokenPô</Title>
      
      <Game countWin={countWin} countDraw={countDraw} countLoose={countLoose}
        setCountWin={setCountWin} setCountDraw={setCountDraw} setCountLoose={setCountLoose}
        setVisible={setVisible} setTurn={setTurn} yourTurn={yourTurn}
        yourChoice={yourChoice} macChoice={macChoice}
        setYourChoice={setYourChoice} setMacChoice={setMacChoice}
      />
      <Score countWin={countWin} countDraw={countDraw} countLoose={countLoose} />

      {visible ? <NewGame setVisible={setVisible} setTurn={setTurn} setYourChoice={setYourChoice} setMacChoice={setMacChoice} /> : null}
    </BodyStyle>
  )
}

export default App;
