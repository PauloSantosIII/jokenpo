import styled from 'styled-components'

export const BodyStyle = styled.body`
    text-align: center;
    background: var(--bg-color);
    min-height: 100vh;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-around;

    @media (max-width: 600px) {
        flex-direction: column;
        justify-content: center;
    }    
`

export const Title = styled.h1`
    color: var(--ft-color);
    font-size: calc(10px + 10vmin);
    position: fixed;
    top: 0;
    margin: 0 auto;
`

export const Board = styled.div`
    color: var(--ft-game);
    border: 3px solid var(--ft-game);
    border-radius: 10px;
    margin-top: 1%;
    padding: 0px 30px;
    width: 40%;
    display: flex;
    flex-flow: row wrap;
    align-items: center;
    justify-content: space-around;

    h1 {
        font-size: calc(5px + 5vmin);
        width: 100%;
    }

    p {
        font-size: calc(15px + 15vmin);
        margin: 0;
        font-style: italic;
    }

    div {
        width: 30%;
        margin: 0;
    }

    span {
        margin: 0;
        font-size: calc(5px + 5vmin);
    }

    @media (max-width: 600px) {
        width: 70%;
        margin: 5%;

        p {
        font-size: calc(16px + 6vmin);
        }
        
        span {
            font-size: calc(4px + 4vmin);
        }
    }
`
export const StyledButton = styled.button`
    background: var(--ft-color);
    border: 3px solid var(--bg-game);
    color: var(--bg-game);
    width: 200px;
    height: 100px;
    font-size: calc(10px + 2vmin);
    font-weight: bolder;
    border-radius: 25px;
    position: fixed;
    bottom: 5%;

    @media (max-width: 600px) {
        width: 100px;
        height: 70px;
    }
`